import numpy as np
import matplotlib.pyplot as plt
import astropy.io.ascii as ascii
import astropy.units as u
from astropy.time import Time
from astropy.table import Column, Table
from glob import glob
import seaborn as sns

import pdb

import os

from jwstuser import engdb
from scipy.signal import medfilt


plt.ion()
plt.close('all')
# Open a user-defined api_token.txt file containing the MAST API token:
api_token = open('api_token.txt','r').readline()[:-1]  #os.getenv('MAST_API_TOKEN')

EDB = engdb.EngineeringDatabase(mast_api_token = api_token)

#target = 'WASP-43b'
#start = '2022-11-30T00:39:52'
#end = '2022-12-02T03:23:36'
#nframe = 64

#target =  'GJ1214b'
#start = '2022-07-19T14:51:58'
#end = '2022-07-22T08:20:42'
#nframe = 42

#target = 'L168-9b'
#start = '2022-05-28T08:08:45.566'
#end = '2022-05-29T12:17:09.045'
#nframe = 9

#target = 'WASP-80b_1'
#start = '2022-09-23T01:41:17'
#end = '2022-09-24T07:46:23'
#nframe = 29

#target = 'WASP-80b_2'
#start = '2022-09-24T14:20:42'
#end = '2022-09-25T20:25:54'
#nframe = 29

#target = 'HD-189733b-1'
#start = '2022-09-15T13:01:58'
#end = '2022-09-16T18:17:32'
#nframe = 5

#target = 'HD-189733b-2'
#start = '2022-09-23T08:27:07'
#end = '2022-09-24T13:06:20'
#nframe = 5

target = 'HD-189733b-3'
start = '2022-10-17T17:34:57'
end = '2022-10-18T22:27:26'
nframe = 5

kernel_size = int(np.ceil((nframe * 0.159) / 0.064))

# First, get Guide Star X-position over a pre-determined range of time:
mnemonic = 'SA_ZFGGSPOSX'
cen_x = EDB.timeseries(mnemonic, start, end)

# we filter with a 25-wide kernel, as the length of 1 integration = 10 * 0.159 second, and the samples in the data are 64000 microseconds. that means 1 integration is ~ 25 telemetry samples

cen_x_filt = medfilt(cen_x.value, kernel_size=kernel_size)

# Now get Y-position:
mnemonic = 'SA_ZFGGSPOSY'

cen_y = EDB.timeseries(mnemonic, start, end)
cen_y_filt = medfilt(cen_y.value, kernel_size=kernel_size)

# Now get the HGA move data
mnemonic = 'SA_ZHGAUPST'
hga = EDB.timeseries(mnemonic, start, end)

# Get filter wheel position
mnemonic = 'IMIR_HK_FW_CUR_POS'
filter_pos = EDB.timeseries(mnemonic, start, end)
np.savetxt(f'filter_pos_{target}.txt', filter_pos.value, fmt='%s')
np.savetxt(f'timing_filter_pos_{target}.txt', filter_pos.time, fmt='%s')


# Get state of detector, either exposure or not
mnemonic = 'IMIR_IC_ACTIVE_DET_MODE'
exposure = EDB.timeseries(mnemonic, start, end)
np.savetxt(f'exposure_{target}.txt', exposure.value, fmt='%s')
np.savetxt(f'timing_exposure_{target}.txt', exposure.time,  fmt='%s')

# Plot FG:

sns.set_style('ticks')

fig, ax1 = plt.subplots(figsize=[12,4], dpi=300)
ax1.plot(cen_x.time, cen_x_filt - np.median(cen_x_filt), '.-', label = 'Guide Star Pos_X (int filter)', color = 'cornflowerblue')
ax1.plot(cen_y.time, cen_y_filt - np.median(cen_y_filt), '.-', label = 'Guide Star Pos_Y (int filter)', color = 'tomato')
ax1.set_ylim((-0.04, 0.03))

ax2 = ax1.twinx()
ax2.plot(hga.time, hga.value, '--', markersize=4, color='k', alpha=0.5, label='HGA move flag')

plt.xticks(fontsize = 14)
plt.yticks(fontsize = 14)

ax1.set_title(f'FGS Guide Star Position during MIRI TSO observation {target}', fontsize=16)
ax1.set_xlabel('Time (UTC)', fontsize = 16)
ax1.set_ylabel('Position (wrt to median)', fontsize = 16)
ax2.set_ylabel('HGA status', fontsize=16)
ax1.legend(fontsize = 7)
ax2.legend(loc=4, fontsize = 7)

fig.tight_layout()


plt.savefig(f'miri_comm_tso_engdb_{target}.png')

