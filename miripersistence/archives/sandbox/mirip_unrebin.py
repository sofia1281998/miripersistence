# mirip_unrebin

import numpy as np
import matplotlib.pyplot as plt
#######################
#print("unbin by replication not interpolation")
def mirip_unrebin(binned_lightcurve, bin_size, nt, plot=False, verbose=False ):
    nb = len(binned_lightcurve)
    if (verbose): print(f' total size ={nt}, rebinned size ={nb}')
    ###
    reference = np.zeros(nt)
    for i in range(bin_size):
        a = list(np.arange(nb) * bin_size + i)
        for j in range(len(a)):
            b = a[j]
            reference[b] = binned_lightcurve[j]
    #
    #print(b, len(a), reference.shape)
    # Including the last values that have been rejected to create the bins
    division_remainder = np.mod(nt, bin_size)
    for i in range(division_remainder):
        reference[(nb-1) * bin_size + bin_size + i] = binned_lightcurve[nb - 1]
    if(plot):
        plt.figure()
        plt.plot(np.arange(nb)*bin_size, binned_lightcurve, '*', label='binned')
        plt.plot(np.arange(nt), reference, label='unbinned')
        plt.legend()
        plt.savefig('plot_unrebin2.pdf')

    return reference
