# mirip_rebin

import numpy as np
import matplotlib.pyplot as plt
#######################
#print("rebin ")
def mirip_rebin(lightcurve, bin_size, plot=False, verbose=False ):
    nt = len(lightcurve)
    nb = nt//bin_size
    # Gets the remainder of the floor division between lightcurve size and bin size
    division_remainder = np.mod(nt, bin_size)
    if (verbose): print(f' total size ={nt}, rebinned size ={nb}, bin size ={bin_size}')
    ###
    binned_lightcurve = np.zeros(nb)
    tt = np.zeros(nb)
    for i in range(nb):
        j = i * bin_size + division_remainder
        tt[i] = j #+ bin_size//2
        binned_lightcurve[i] = np.mean(lightcurve[j: (j+bin_size)])
    #
    if(plot):
        plt.figure()
        plt.plot(tt+bin_size//2, binned_lightcurve, '+', label='binned', color='green')
        plt.plot(lightcurve, label='unbinned', color='blue')
        for i in np.arange(nb):
            plt.plot([tt[i], tt[i]+bin_size], [binned_lightcurve[i],binned_lightcurve[i]], color='green')
        plt.legend()
        #plt.savefig('plot_unrebin2.pdf')
    return binned_lightcurve
