# select good pixels
##  remove the background from the image, see launch_select_good_pixels
##    bck = image[:,60:71].mean(axis=1)
##    image = image-bck.reshape([416,1])
#
## date 24 April 2023, Author R Gastaud  cea
#
import numpy as np
from skimage.morphology import erosion, dilation, area_opening
from matplotlib import pyplot as plt
def select_good_pixels(image, k=3, plot=False, verbose=False, ypos=11):
    # first loop
    my_median = np.median(image)
    ##  to get rid of bad pixel on the first columns (left)
    image[:,0:ypos] = my_median
    my_deviation = np.median(np.abs(image-my_median))*np.sqrt(np.pi/2)
    threshold1 = my_median + k*my_deviation
    good_pixels1 = np.where(image > threshold1)
    if(verbose): print(my_median, my_deviation, threshold1, len(good_pixels1[0]))
    my_mask1 = np.zeros(image.shape)
    my_mask1[good_pixels1]=1
    if(plot):
        plt.title("first mask, threshold={:.2e}".format(threshold1))
        plt.imshow(my_mask1, origin='lower')
        plt.savefig('first_mask.png')
    #  re iterate
    bck_pixels = np.where(image < threshold1)
    bck_image = image[bck_pixels]
    my_median = np.median(bck_image)
    my_deviation = np.median(np.abs(bck_image-my_median))*np.sqrt(np.pi/2)
    threshold2 = my_median + k*my_deviation
    good_pixels2 = np.where(image > threshold2)
    if(verbose): print(my_median, my_deviation, threshold2, len(good_pixels2[0]))
    my_mask2 = np.zeros(image.shape)
    my_mask2[good_pixels2]=1
    if(plot):
        plt.title("second mask , threshold={:.2e}".format(threshold2))
        plt.imshow(my_mask2, origin='lower')
        plt.savefig('second_mask.png')
        #plt.show()
    #
    
    my_mask3 = area_opening(my_mask2, 7)#, kernel)
    if(plot):
        plt.title("third mask , erosion")
        plt.imshow(my_mask3, origin='lower')
        #plt.show()
        plt.savefig('third_mask.png')
    #
    kernel = np.ones([3,3])
    my_mask4 = dilation(my_mask3, kernel)
    if(plot):
        plt.title("4th mask , dilatation by 3x3 kernel")
        plt.imshow(my_mask4, origin='lower')
        #plt.show()
        plt.savefig('fourth_mask.png')
    #
    return my_mask4
