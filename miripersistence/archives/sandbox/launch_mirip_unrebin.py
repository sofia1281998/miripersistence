# Launch mirip_unrebin

from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
#######################
from mirip_unrebin import mirip_unrebin
####
bin_size = 30
binned_lightcurve = fits.getdata('binned_lightcurve_v0.fits')
pixels_time_series = fits.getdata('pixels_time_series_v0.fits')
############
nt, nxy = pixels_time_series.shape
print(f'pixels_time_series.shape nt={nt} nxy={nxy}')

###
print("unbin by replication not interpolation")
reference = np.zeros(nt)
l = len(binned_lightcurve)
for i in range(bin_size):
    a = list(np.arange(l) * bin_size + i)
    for j in range(len(a)):
        b = a[j]
        reference[b] = binned_lightcurve[j]
#
print(b, len(a), reference.shape)

# Including the last values that have been rejected to create the bins
division_remainder = np.mod(nt, bin_size)
for i in range(division_remainder):
    reference[(l-1) * bin_size + bin_size + i] = binned_lightcurve[l - 1]

toto = mirip_unrebin(binned_lightcurve, bin_size, nt, plot=False )

diff = toto - reference
print(diff.min(), diff.max())
#0.0 0.0

toto = mirip_unrebin(binned_lightcurve, bin_size, nt, plot=True, verbose=True )


