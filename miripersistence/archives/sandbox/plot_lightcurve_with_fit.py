# plots

import numpy as np
from matplotlib import pyplot as plt
import os
from astropy.io import fits
from mirip_read_rateints import mirip_read_rateints
from fit_two_exponentials import persistence

output_filename1 = '/Users/gastaud/achrene/result_10.fits'
input_dir = '/Users/gastaud/achrene/L168-9b_data/'
pattern = 'jw01033005001_04103_00001-seg00?_mirimage_rateints.fits'
#  index of the transit
i1 = 5044
i2 = 7938
cube, tt, target_prop, pid = mirip_read_rateints(input_dir, pattern='*.fits', verbose=True)
hdul = fits.open(output_filename1)
hdul.info()
index_x = hdul['index_x'].data
index_y = hdul['index_y'].data
flux = hdul[0].data
header = hdul[0].header
print(repr(header))
params  = hdul[0].data
print(cube.shape,params.shape)

for i in np.arange(12):
    x = params[i,:]
    result = "flux={:5.2f}, alpha1={:5.2f}, tau1={:5.2f}, alpha2={:5.2f}, tau2={:5.2f}".format(x[0], x[1], x[2], x[3], x[4])
    print(i, result)
for i in np.arange(12):
    print(i, cube[:, index_y[i], index_x[i]].mean())
    
i=9
params.shape
x = params[9,:]
result = "flux={:5.2f}, alpha1={:5.2f}, tau1={:5.2f}, alpha2={:5.2f}, tau2={:5.2f}".format(x[0], x[1], x[2], x[3], x[4])
print(result)

y_theoric = persistence(x, tt)
mytime=tt

plt.figure()
plt.plot(mytime,cube[:, index_y[i], index_x[i]], '.', label='measured points')
plt.plot(mytime, y_theoric, label='Fit by two exponentials')
plt.legend()
plt.title('pid='+pid+' target='+target_prop+' col='+str(index_x[i]) +' row='+str(index_y[i]) +'\n'+result)
plt.xlabel('Time second')
plt.ylabel('Flux DN/s')
plt.show()
plt.savefig('plot_compare.jpeg')
