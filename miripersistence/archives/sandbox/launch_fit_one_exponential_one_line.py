import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import os

###################
def persistence(parameters, t):
    a = parameters[0]
    b = parameters[1]
    tau = parameters[2]
    return a + b * np.exp(-t/ tau)

def minus_log_likelihood(parameters, t, y, yerr):
    y_theoric = persistence(parameters, t)
    sigma2 = yerr ** 2
    ecart = -0.5 * np.sum((y - y_theoric) ** 2 / sigma2)
    return -ecart

#####################

###  initialisation
pid = '1033'
target ='L168-9'
input_folder = 'data/'
flux = np.loadtxt(os.path.join(input_folder, 'newY_data.txt'))
mask = np.loadtxt(os.path.join(input_folder, 'newY_mask.txt'))
mytime = np.loadtxt(os.path.join(input_folder, 'binned_time.txt'))
initial = np.array([ 4.334e+06,  10.,  1.000e+03])
fluxerr=0.001
###  end of initialisation

index = np.where(mask == False)
index = index[0]
plt.plot(mytime, flux)
plt.plot(mytime[index], flux[index], '.')
plt.show()
index = np.where(mask == True)
index = index[0]
print(index.min(), index.max(), index.shape)
#160 259 (100,)
print(flux[index].mean(), flux[index].std())
# 5806. 1.90


soln = minimize(minus_log_likelihood, initial, args=(mytime[index], flux[index], fluxerr), method='Nelder-mead',options={'atol' : 1e-8, 'maxiter' : 10000, 'disp' : True, 'adaptive' : True})
print(soln)
params_mle = soln.x
conv = soln.success
result = "flux={:5.2f}, alpha={:5.2f}, tau={:5.2f}".format(params_mle [0], params_mle [1], params_mle [2])
print(result)
# fluxerr=1. flflux=5805.99, alpha=12.33, tau=1089.18
#
y_theoric = persistence(params_mle, mytime)

plt.plot(mytime, y_theoric, label='Fit by one exponential \n'+result)
plt.plot(mytime[index], flux[index], '.', label='measured points')
plt.legend()
plt.title('pid='+pid+' target='+target)
plt.xlabel('Time second')
plt.ylabel('Flux DN/s')
#plt.savefig('plot_one_exponential.pdf')
plt.show()
