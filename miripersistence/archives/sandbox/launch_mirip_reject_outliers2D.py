# launch_mirip_reject_outliers2D

from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import median_filter
#######################
from mirip_unrebin import mirip_unrebin
from mirip_reject_outliers2D import mirip_reject_outliers2D
####
bin_size = 30
#binned_lightcurve = fits.getdata('binned_lightcurve_v0.fits')
pixels_time_series = fits.getdata('pixels_time_series_v0.fits')
nx = 61 # 72-11
ny = 403
############
nt, nxy = pixels_time_series.shape
print(f'pixels_time_series.shape nt={nt} nxy={nxy}')

####  check the shape of pixels_time_series
last_image = pixels_time_series[-1,:]
last_image = last_image.reshape([ny, nx])
plt.figure('last image')
plt.imshow(last_image)
plt.show()
#
imagette = last_image[-100:,36-11-10:36]
print(imagette.shape)
nny, nnx = imagette.shape
plt.figure('last imagette')
plt.imshow(imagette)
plt.show()
##  create a sub cube = cubette to speed the test
cube = pixels_time_series.reshape([nt, ny, nx])
cubette = cube[:, -100:,36-11-10:36]
pixels_time_seriettes = cubette.reshape([nt, nny*nnx])
#
##  compute the new reference
lc = np.sum(cubette, (1,2))
# smooth the ligth curve to create the reference
referencette = median_filter(lc, bin_size)
plt.plot(lc, label='lc')
plt.plot(referencette, label='referencette')
plt.legend()
plt.show()

###  now call the function to test
corrected_lightcurve, corrected_full_lightcurve = mirip_reject_outliers2D(pixels_time_seriettes, referencette)

diff = corrected_full_lightcurve - referencette
print("corrected_full_lightcurve - reference", diff.min(), diff.max())
plt.figure('corrected_full_lightcurve - reference')
plt.plot(corrected_full_lightcurve, label='corrected_full_lightcurve')
plt.plot(referencette, label='referencette')
plt.legend()
plt.show()

diff = corrected_lightcurve - pixels_time_seriettes
print("corrected_lightcurve - pixels_time_seriettes", diff.min(), diff.max())
index = np.where(np.abs(diff) > 1)
print(len(index[0])/diff.size*100)
# 0.10451697486165525

##  show an outlier
(it, iyx) = np.unravel_index(np.argmax(np.abs(diff)), diff.shape)

plt.figure('show an outlier')
plt.plot(pixels_time_seriettes[it-20:it+20, iyx], '*', label='pixels_time_series')
plt.plot(corrected_lightcurve[it-20:it+20, iyx],  label='corrected_lightcurve')
plt.legend()
plt.show()

