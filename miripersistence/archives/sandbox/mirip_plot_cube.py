# IPython log file
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib as mpl

def mirip_plot_cube(spectrum_zone, tt, target, pid, output_folder):

    mpl.rcParams['font.size'] = 4.0
    mpl.rcParams['lines.linewidth'] = 0.5
    mpl.rcParams['lines.markersize'] = 3

    nt, ny, nx = spectrum_zone.shape
    print(f"Chosen zone's shape is: nt:{nt}, ny:{ny} and nx{nx}")

    #######  last spectral image
    fig, ax = plt.subplots(figsize=(2, 3), dpi=300)
    im = ax.imshow(spectrum_zone[-1,:,:], interpolation='nearest', aspect='auto', cmap=plt.cm.plasma, alpha=.9, norm=colors.LogNorm(), origin='lower')
    # replace flip by origin='lower'
    plt.colorbar(im)
    plt.title('Last Spectral Image for '+target+', pid:'+pid)
    plt.savefig(os.path.join(output_folder, 'spectral_image.png'))

    #############  light curve
    lightcurve = np.sum(spectrum_zone, axis=(1, 2))
    #lightcurve = np.sum(full[:, 7:395, 36-5:36+6], (1,2))
    plt.figure(figsize=(5, 2), dpi=300)
    plt.scatter(tt, lightcurve, s=10, alpha=0.5, color='blue')
    plt.title('White light-curve for '+target+', pid:'+pid)
    plt.xlabel('Time [s]')
    plt.ylabel('Flux [DN/s]')
    plt.tight_layout()
    plt.savefig(os.path.join(output_folder, 'white_lightcurve.png'))

    return
