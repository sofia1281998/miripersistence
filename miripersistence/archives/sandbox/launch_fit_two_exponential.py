import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import os

###################
def persistence(parameters, t):
    a = parameters[0]
    b = parameters[1]
    tau1 = parameters[2]
    c = parameters[3]
    tau2 = parameters[4]

    return a + b * np.exp(-t/ tau1) + c*np.exp(-t/ tau2)

def minus_log_likelihood(parameters, t, y, yerr):
    y_theoric = persistence(parameters, t)
    sigma2 = yerr ** 2
    ecart = -0.5 * np.sum((y - y_theoric) ** 2 / sigma2)
    return -ecart

#####################

###  initialisation
pid = '1033'
target ='L168-9'
input_folder = 'data/' # 'data/'
flux = np.loadtxt(os.path.join(input_folder, 'newY_data.txt'))
mask = np.loadtxt(os.path.join(input_folder, 'newY_mask.txt'))
mytime = np.loadtxt(os.path.join(input_folder, 'binned_time.txt'))
initial = np.array([ 4.334e+06,  1.000e+01,  1.000e+03, 33, 100])
# BEWARE DEPENDS UPON INITIAL CONDITION
# initial = np.array([ 4.334e+06,  1.000e+01,  1.000e+03, 10, 10])
fluxerr=0.001
###  end of initialisation

index = np.where(mask == False)
index = index[0]
plt.plot(mytime, flux)
plt.plot(mytime[index], flux[index], '.')
plt.show()
bad_index = np.where(mask == True)
bad_index = bad_index[0]
print(bad_index.min(), bad_index.max(), bad_index.shape)
#160 259 (100,)
print(flux[bad_index].mean(), flux[bad_index].std())
# 5806. 1.90
print(flux[bad_index.max():].shape, flux[bad_index.max():].mean(), flux[bad_index.max():].std())
# 53,) 5808. 1.7

soln = minimize(minus_log_likelihood, initial, args=(mytime[index], flux[index], fluxerr), method='Nelder-mead',options={'fatol' : 1e-8, 'maxiter' : 10000, 'disp' : True, 'adaptive' : True})
print(soln)
x = soln.x
conv = soln.success
result = "flux={:5.2f}, alpha1={:5.2f}, tau1={:5.2f}, alpha2={:5.2f}, tau2={:5.2f}".format(x[0], x[1], x[2], x[3], x[4])
print(result)
#
y_theoric = persistence(x, mytime)

plt.plot(mytime, y_theoric, label='Fit by two exponentials')
plt.plot(mytime[index], flux[index], '.', label='measured points')
plt.legend()
plt.title('pid='+pid+' target='+target+'\n'+result)
plt.xlabel('Time second')
plt.ylabel('Flux DN/s')
plt.savefig('plot_two_exponential.pdf')
plt.show()


plt.plot(mytime[index], y_theoric[index] - flux[index], '-o')
plt.show()
