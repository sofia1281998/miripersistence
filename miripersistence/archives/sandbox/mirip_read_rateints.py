#
from astropy.io import fits

import numpy as np
import glob
import os

def mirip_read_rateints(input_folder, pattern='*_rateints.fits', verbose=False):
    files = sorted(glob.glob(os.path.join(input_folder, pattern)))
    nf = len(files)
    if verbose:
        print('number of files : {} '.format(nf))
        #for file in files:print(file)
    nn=0
    for file in files:
        header = fits.getheader(file,1)
        naxis = header['naxis']
        naxis2 = header['naxis2']
        naxis3 = header['naxis3']
        naxis1 = header['naxis1']
        nn = nn+naxis3
        if (verbose):print(file, naxis1, naxis2, naxis3)
    ##########################@
    full = []
    j=0
    for file in files:
        data = (fits.getdata(file))
        full.extend(data)
        if (verbose):print(j, nf)
        j +=1
    full = np.asarray(full, dtype=np.float32)
    ###
    header = fits.getheader(files[0])
    nresets = header['nresets']
    ngroups = header['ngroups']
    tframe = header['tframe']
    duration = header['duration']
    nints = header['nints']  ## should be equal to nn
    pid  = header['PROGRAM']
    integration_period = tframe*(ngroups+nresets )
    print(duration, nints, integration_period, tframe)
    target_prop = header['TARGPROP']
    tt = np.arange(nints)*integration_period
    if(verbose):print('shape', full.shape, 'nints', nints, 'nn', nn)
    return full, tt, target_prop, pid
