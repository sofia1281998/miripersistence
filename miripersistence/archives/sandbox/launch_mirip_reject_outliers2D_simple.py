# IPython log file

from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import median_filter
#######################
from mirip_reject_outliers2D import mirip_reject_outliers2D
####
binned_lightcurve = fits.getdata('binned_lightcurve_v0.fits')
nt = binned_lightcurve.size
pixels_time_series = np.zeros([nt, 4])
for i in np.arange(4):pixels_time_series[:,i]=binned_lightcurve
print(binned_lightcurve.min(), binned_lightcurve.mean(), binned_lightcurve.max())

delta = binned_lightcurve.max()-binned_lightcurve.min()
pixels_time_series[nt//2,0] = binned_lightcurve[nt//2]+delta
corrected_lightcurve, corrected_full_lightcurve = mirip_reject_outliers2D(pixels_time_series, 4*binned_lightcurve)
##  no correction !
plt.figure('1*delta')
plt.plot(pixels_time_series[:,0], label='before')
plt.plot(binned_lightcurve, label='reference')
plt.plot(corrected_lightcurve[:,0], label='after')
plt.legend()
plt.xlim([nt//2-10, nt//2+10])
plt.savefig('my_plot_10.pdf')
plt.show()

#########
pixels_time_series[nt//2,0] = binned_lightcurve[nt//2]+5*delta
corrected_lightcurve, corrected_full_lightcurve = mirip_reject_outliers2D(pixels_time_series, 4*binned_lightcurve)
plt.figure('5*delta')
plt.plot(binned_lightcurve, label='reference')
plt.plot(pixels_time_series[:,0], label='before')
plt.plot(corrected_lightcurve[:,0], label='after')
plt.legend()
plt.xlim([nt//2-10, nt//2+10])
plt.savefig('my_plot_11.pdf')
plt.show()
