# IPython log file

from astropy.io import fits
import numpy as np
import glob
import os
######
from mirip_plot_cube import mirip_plot_cube
######
filename='/Users/gastaud/exoplanets/commissioning/pid1033_rateints/jw01033005001_04103_00001-seg004_mirimage_rateints.fits'
######
cube = fits.getdata(filename)
nt, ny, nx = cube.shape
print(cube.shape)
tt = np.arange(nt)
mirip_plot_cube(cube, tt, 'L-168-9', '01033', 'tmp/')
# Window size
x1= 11 #11
x2= 72 #72
y1= 0 #0
y2= 403
spectrum_zone = cube[:, y1:y2,x1:x2]
mirip_plot_cube(spectrum_zone, tt, 'L-168-9', '01033', 'tmp/')

