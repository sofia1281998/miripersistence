#  Detect start of the transit i1, end of the transit i2

import numpy as np
import os
import matplotlib.pyplot as plt
from matplotlib import colors
from mirip_read_rateints import mirip_read_rateints
from scipy.ndimage import median_filter
########################
#######  INITIALISATION ##############
### spatial box: depends only on QE, DISPERSION, Filter Transmission
##         and shape of the spectrum of the star  (Rayleigh Jeans 1/wavelength**2)
x1=33
x2=39
#  (x1+x2)  = 72
y1=200
y2=399

####  Input data
input_folder='/Users/gastaud/exoplanets/commissioning/pid1033_rateints/'
pattern='jw01033005001_04103_00001-seg00?_mirimage_rateints.*'
bin_size = 100 # < duration of the transit


####  read ####
cube, tt, target, pid = mirip_read_rateints(input_folder, pattern=pattern, verbose=True)

###  Check the spatial window

image = cube.mean(axis=0)
my_line = image[:,36]
flux_r = [my_line.min(), my_line.max()]
plt.figure()
plt.plot(my_line)
plt.plot([y1,y1], flux_r)
plt.plot([y2,y2], flux_r)
plt.show()


my_line = image[387,:]
flux_r = [my_line.min(), my_line.max()]
plt.figure()
plt.plot(image[387,:])
plt.plot([x1,x1], flux_r)
plt.plot([x2,x2], flux_r)
plt.show()


## spatial mean
my_line = cube[:, y1:y2, x1:x2].mean(axis=(1,2))

# time smoothing
smoothed_line = median_filter(my_line, bin_size)

plt.figure()
plt.plot(my_line, '.', label='spatial mean')
plt.plot(smoothed_line , label='spatial mean and time smoothed')
plt.legend()
plt.show()

####  detect i1, i2
my_median = np.median(smoothed_line)
nt = smoothed_line.size
ecart = np.median(np.abs(smoothed_line-my_median))
ii = np.where(smoothed_line < (my_median-ecart))
ii = ii[0]
print(ii.min(), ii.max(), ii.max()-ii.min(), len(ii))
# 5112 7814  2702 2701
i1=ii.min()
i2 = ii.max()

########  plot : visual verification
yrange = [smoothed_line.min(), smoothed_line.max()]
plt.figure()
plt.plot(smoothed_line)
plt.plot([0,nt], [my_median-ecart,my_median-ecart])
plt.plot([i1,i1], yrange)
plt.plot([i2,i2], yrange)
plt.savefig('plot_i1_i2.png')
plt.show()
