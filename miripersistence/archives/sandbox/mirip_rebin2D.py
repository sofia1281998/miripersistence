# mirip_rebin

import numpy as np
import matplotlib.pyplot as plt
#######################
#print("rebin ")
def mirip_rebin2D(lightcurve, bin_size, plot=False, verbose=False ):
    nt, nxy = lightcurve.shape
    nb = nt//bin_size
    # Gets the remainder of the floor division between lightcurve size and bin size
    division_remainder = np.mod(nt, bin_size)
    if (verbose): print(f' total size ={nt}, rebinned size ={nb}, bin size ={bin_size}')
    ###
    binned_lightcurve = np.zeros([nb, nxy])
    tt = np.zeros(nb)
    for i in range(nb):
        j = i * bin_size + division_remainder
        tt[i] = j #+ bin_size//2
        binned_lightcurve[i, :] = np.mean(lightcurve[j: (j+bin_size), :], axis=0)
    #
    if(plot):
        plt.figure()
        plt.plot(tt+bin_size//2, binned_lightcurve[:, 0], '+', label='0-binned', color='green')
        plt.plot(lightcurve[:, 0], label='0-unbinned', color='blue')
        for i in np.arange(nb):
            plt.plot([tt[i], tt[i]+bin_size], [binned_lightcurve[i,0],binned_lightcurve[i,0]], color='green')
        plt.plot(tt+bin_size//2, binned_lightcurve[:, 1], '+', label='1-binned')
        plt.plot(lightcurve[:, 1], label='1-unbinned')
        plt.legend()
        #plt.savefig('plot_unrebin2.pdf')
    return binned_lightcurve
