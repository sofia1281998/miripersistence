#
#

import numpy as np
import matplotlib.pyplot as plt
import copy
from itertools import groupby
from operator import itemgetter
from matplotlib import colors
import matplotlib as mpl
import matplotlib.lines as lines


def mirip_reject_outliers2D(pixels_time_series, reference):
 
    nt, nxy = pixels_time_series.shape
    print(f'pixels_time_series.shape nt={nt} nxy={nxy}')
    full_length = nt - 2
    outliers = np.zeros(1)
    lightcurve = np.sum(pixels_time_series, axis=1)

    # ??? Corrects the binned data, getting rid of the outlier values
    ##  not binned !
    corrected_lightcurve = copy.deepcopy(pixels_time_series)


    while len(outliers) != 0:
        std_deviation = []
        outliers_ids = []
        outliers = []
        outlier_groups = []
        pre_ids = []
        post_ids = []
        
        for i in range(2, full_length):
            tmp_sd = np.std(lightcurve[(i - 2):(i + 3)])
            std_deviation.append(tmp_sd)

        print(f'The moving std deviation vector length is: ',len(std_deviation))

        #  Gets the  median value  of the  moving standard deviation
        median_std = np.nanmedian(std_deviation)
        # Gets the outliers values and IDS
        for i in range(2, full_length):
            if np.abs(lightcurve[i] - reference[i]) > 5.0 * median_std:
                outliers.append(lightcurve[i])
                outliers_ids.append(i)

        print(f'There are {len(outliers)} outliers in the full lightcurve.')


        for k, g in groupby(enumerate(outliers_ids), lambda x: x[1] - x[0]):
            tmp_groups = np.asarray(list(map(itemgetter(1), g)))
            outlier_groups.append(tmp_groups)
            
        lower_limit = 0
        upper_limit = nt - 1

        for group in outlier_groups:
            vector = group - len(group)
            vector[np.where(vector < lower_limit)[0]] = lower_limit
            pre_ids.append(vector)

        for group in outlier_groups:
            vector = group + len(group)
            vector[np.where(vector > upper_limit)[0]] = upper_limit
            post_ids.append(vector)


        #corrected_binned_lightcurve = np.zeros(len(binned_lightcurve))
        len_groups = len(outlier_groups)

        for i in range(len_groups):
            for j in range(len(outlier_groups[i])):
                corrected_lightcurve[outlier_groups[i][j], :] = 0.5 * (corrected_lightcurve[pre_ids[i][j], :] + corrected_lightcurve[post_ids[i][j], :])

        corrected_full_lightcurve = np.sum(corrected_lightcurve, axis=1)

        # Replaces the initial data by the corrected ones to search the remaining outliers
        lightcurve = corrected_full_lightcurve
    return corrected_lightcurve, corrected_full_lightcurve


