#   MAIN script
# Author : R Gastaud

# From the cdp mask extract the subarray corresponding to MIRI LRS SLITLESS
# date :  20 April 2023
#  26 avr 2023 : add rebin

## May, 8th 2023 : good pixels with k=5 and rebin with bin_size=12 for version 1

import numpy as np
from matplotlib import pyplot as plt
import os
from astropy.io import fits
from scipy.ndimage import median_filter
from scipy.optimize import minimize
import time
import sys
###############  home-made functions ######
# library_dir = '/Users/gastaud/exoplanets/miripersistence/miripersistence/archives/sandbox/'
# sys.path.append(library_dir)
from mirip_read_rateints import mirip_read_rateints
from select_good_pixels import select_good_pixels
#from fit_two_exponentials import fit_two_exponentials
from launch_fit_one_exponential_one_line import persistence
from launch_fit_one_exponential_one_line import minus_log_likelihood

#from mirip_rebin import mirip_rebin
from mirip_rebin1d import mirip_rebin1d
from deglitch import deglitch
#######################################

#############  initialisation  ############
# fit_two_exponentials
mymodel = 'a + b * np.exp(-t/ tau1)'
n_p = 3 # number of parameters of fit_one_exponential
output_filename1 = 'result1d_fit_one_exponential_v1.fits'
output_filename2 = 'result3d_fit_one_exponential_v1.fits'

cal_dir = '/Users/gastaud/exoplanets/miripersistence/miripersistence/archives/sandbox/data/'
mask_filename= 'maskette.fits'

## input_folder='/Users/gastaud/exoplanets/commissioning/pid1033_rateints/'
## pattern='jw01033005001_04103_00001-seg00?_mirimage_rateints.*'
input_dir = '/Users/gastaud/achrene/L168-9b_data/'
pattern = 'jw01033005001_04103_00001-seg00?_mirimage_rateints.fits'
#  index of the transit
i1 = 5044
i2 = 7938
verbose = True
bin_size = 12
#############  end of initialisation  ###########

start_time = time.time()
##########     read the input FITS files ###################
cube, mytime, target_prop, pid = mirip_read_rateints(input_dir, pattern='*.fits', verbose=True)

ligne =  cube[:,200:383,36-2:36+3].mean(axis=(1,2))
fligne = median_filter(ligne,100)
yrange = [fligne.min(), fligne.max()]
plt.plot(fligne)
plt.plot([i1,i1], yrange)
plt.plot([i2,i2], yrange)
plt.show()

##########    select good pixels ###################
image = cube.mean(axis=0)
if verbose:print('image', image.shape)
## clean the image
# done by the function image[:, 0:11] = np.median(image)
cleaned_image = image.copy()
cleaned_image[0:2,:] = np.median(image)
ligne = cleaned_image[:,64:70].mean(axis=1)
cleaned_image = cleaned_image - ligne.reshape([416,1])
good_pixels = select_good_pixels(cleaned_image, k=5, plot=False, verbose=False, ypos=11)
mask = fits.getdata(os.path.join(cal_dir, mask_filename))
index = np.where(np.logical_and((good_pixels == 1), (mask == 0)))


##########    FIT THE MODEL ###################

nn = len(index[0])
fluxerr = 0.001
initial = np.array([ 27963.,  746.,    80.])

result = np.zeros([nn, n_p+2])
mytime = mirip_rebin1d(mytime, bin_size)
lmask = np.zeros(mytime.shape)
lmask[i1//bin_size:i2//bin_size]=1
time_index = np.where(lmask == 0)
time_index = time_index[0]
print('number of pixels', nn)
for i in np.arange(nn):
    elapsed_time = time.time() - start_time
    if (i%100 == 0):print(i, elapsed_time, nn)
    flux = cube[:, index[0][i], index[1][i]]
    initial[0] = np.median(flux)
    glitches_index = deglitch(flux)
    flux[glitches_index] = np.nan
    flux = mirip_rebin1d(flux, bin_size)
    kk = np.where(np.isnan(flux))
    if( kk[0] > 0):print("erreeeeur")
    soln = minimize(minus_log_likelihood, initial, args=(mytime[time_index], flux[time_index], fluxerr), method='Nelder-mead',options={'atol':1e-8, 'maxiter':10000, 'disp':False, 'adaptive':True})
    result[i, 0:n_p] = soln.x
    result[i, n_p] = soln.nit
    y_theoric = persistence(soln.x, mytime)
    erreur = np.std(flux[time_index] - y_theoric[time_index])
    result[i, n_p+1] = erreur
##########    WRITE THE RESULT  ###################

hdu0 = fits.PrimaryHDU(result)
hdu1 = fits.ImageHDU(index[0])
hdu2 = fits.ImageHDU(index[1])

hdu0.header['MODEL'] = mymodel+' version 1'
hdu0.header['bin_size'] = bin_size
hdu1.header['EXTNAME'] = 'INDEX_Y'
hdu2.header['EXTNAME'] = 'INDEX_X'

hdul = fits.HDUList([hdu0, hdu1, hdu2])
hdul.writeto(output_filename1, overwrite=True)

##########   another  WRITE  ###################
nz, ny, nx = cube.shape
result3d = np.zeros([n_p+2, ny, nx])
result3d[:] = np.nan
for i in np.arange(nn):
    result3d[:, index[0][i], index[1][i]]  = result[i, :]
hdu = fits.PrimaryHDU(result3d)
hdu.header['MODEL'] = mymodel+' version 1'
hdu0.header['bin_size'] = bin_size
hdu.writeto(output_filename2, overwrite=True)

elapsed_time = time.time() - start_time
print ('elapsed time: {}'.format(elapsed_time), mymodel, n_p, nn)



