import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import os

###################
def persistence(parameters, t):
    a = parameters[0]
    b = parameters[1]
    tau1 = parameters[2]
    c = parameters[3]
    tau2 = parameters[4]

    return a + b * np.exp(-t/ tau1) + c*np.exp(-t/ tau2)

def minus_log_likelihood(parameters, t, y, yerr):
    y_theoric = persistence(parameters, t)
    sigma2 = yerr ** 2
    ecart = 0.5 * np.sum((y - y_theoric) ** 2 / sigma2)
    return ecart

#####################

def fit_two_exponentials(flux, mask, mytime, initial, fluxerr, plot=False, verbose=False):
###  initialisation
    index = np.where(mask == False)
    index = index[0]
    plt.plot(mytime, flux)
    if(plot):
        plt.plot(mytime[index], flux[index], '.')
        plt.show()
    bad_index = np.where(mask == True)
    bad_index = bad_index[0]

    soln = minimize(minus_log_likelihood, initial, args=(mytime[index], flux[index], fluxerr), method='Nelder-mead',options={'fatol':1e-8, 'maxiter':10000, 'disp':False, 'adaptive':True})   # disp=False  do not print convergence messages.
         # fatol Absolute error in func(xopt) between iterations that is acceptable for convergence
         #  for Nelder Mead (simplex)

    if verbose: print(soln)
    x = soln.x
    conv = soln.success
    result = "flux={:5.2f}, alpha1={:5.2f}, tau1={:5.2f}, alpha2={:5.2f}, tau2={:5.2f}".format(x[0], x[1], x[2], x[3], x[4])
    if verbose: print(result)
    #
    y_theoric = persistence(x, mytime)
    if(plot):
        plt.figure()
        plt.plot(mytime, y_theoric, label='Fit by two exponentials')
        plt.plot(mytime[index], flux[index], '.', label='measured points')
        plt.legend()
        plt.title('pid='+pid+' target='+target+'\n'+result)
        plt.xlabel('Time second')
        plt.ylabel('Flux DN/s')
        plt.savefig('plot_two_exponential.pdf')
        plt.show()
        plt.figure()
        plt.plot(mytime[index], y_theoric[index] - flux[index], '-o')
        plt.show()
    return x
 
