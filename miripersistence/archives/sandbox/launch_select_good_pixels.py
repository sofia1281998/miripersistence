# Launch select good pixels
#
## date 24 April 2023, Author R Gastaud  cea
#
import skimage
import numpy as np
from astropy.io import fits
from matplotlib import pyplot as plt
##
from select_good_pixels import select_good_pixels
##
import sys
sys.path.append('/Users/gastaud/exoplanets/miripersistence/miripersistence/archives/sandbox')
from mirip_read_rateints import mirip_read_rateints

#cube = fits.getdata('big_slopes.fits')
input_dir='/Users/gastaud/achrene/L168-9b_data/'
###
cube, tt, target_prop, pid = mirip_read_rateints(input_dir, pattern='*.fits', verbose=True)
image = cube.mean(axis=0)
print('image.shape', image.shape)
## clean the image
image[:, 0:11] = np.median(image)
image[0:2,:] = np.median(image)
plt.imshow(image, origin='lower')
plt.show()
## remove the background
ligne = image[:,60:71].mean(axis=1)
result = image-ligne.reshape([416,1])
print(ligne.shape, result.shape)

mask=select_good_pixels(result, k=3, plot=True, verbose=True, ypos=11)


