# launch mirip_reject_outliers1d

from mirip_reject_outliers1D import mirip_reject_outliers1D
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt

##
data0 = fits.getdata('binned_lightcurve_v0.fits')
data1 = fits.getdata('binned_lightcurve_v1.fits')
diff = data0-data1
print(diff.min(), diff.max())
###
binned_lightcurve = data0
corrected_binned_lightcurve = mirip_reject_outliers1D(binned_lightcurve)
diff = binned_lightcurve - corrected_binned_lightcurve
print('binned_lightcurve - corrected_binned_lightcurve', diff.min(), diff.max())
##  0,0

### add an outlier
print('new test with an outlier')
nn = binned_lightcurve.size
my_sigma = np.std(binned_lightcurve)
new_binned_lightcurve = np.copy(binned_lightcurve)
new_binned_lightcurve[nn//2] = binned_lightcurve[nn//2] +5*my_sigma
print( binned_lightcurve.min(), binned_lightcurve.mean(), binned_lightcurve.max())
print(my_sigma)
new_corrected_binned_lightcurve = mirip_reject_outliers1D(new_binned_lightcurve)
diff = new_binned_lightcurve - new_corrected_binned_lightcurve
print('diff new_binned_lightcurve - new_corrected_binned_lightcurve',diff.min(), diff.max())

diff = binned_lightcurve - new_corrected_binned_lightcurve
print('binned_lightcurve - new_corrected_binned_lightcurve', diff.min(), diff.max())

plt.figure()
plt.plot(binned_lightcurve, label='binned_lightcurve')
plt.plot(new_binned_lightcurve, label='new_binned_lightcurve')
plt.plot(new_corrected_binned_lightcurve, label='new_corrected_binned_lightcurve')
plt.legend()
plt.xlim([nn//2-10,nn//2+10])
plt.savefig('my_plot.pdf')
 
