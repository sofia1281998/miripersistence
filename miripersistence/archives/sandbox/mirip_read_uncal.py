#  READ UNCAL FITS FILES
#  see alose mirip_read_rateints

import numpy as np
import matplotlib.pyplot  as plt
from astropy.io import fits
import os
import glob
###################################
#input_dir='/Users/gastaud/exoplanets/commissioning/pid1033/'
#pattern='jw01033005001_04103_00001-seg00?_mirimage_uncal.fits'

def mirip_read_uncal(input_dir, pattern='*_uncal.fits', verbose=False):
    files = sorted(glob.glob(os.path.join(input_dir, pattern)))
    nf = len(files)
    print('number of files : {} '.format(nf))
    nn=0
    j=0
    ni = np.zeros(nf+1, dtype='int')
    for file in files:
        header = fits.getheader(file,1)
        naxis = header['naxis']
        naxis2 = header['naxis2']
        naxis3 = header['naxis3']
        naxis4 = header['naxis4']
        naxis1 = header['naxis1']
        nn = nn+naxis4
        j = j+1
        ni[j]=nn
        if verbose: print(j, file, naxis1, naxis2, naxis3, naxis4)
    ##########################
    j=0
    hypercube = np.zeros([nn, naxis3, naxis2, naxis1])
    hypercube[:] = np.nan
    for file in files:
        data = (fits.getdata(file)).astype(float)
        hypercube[ni[j]:ni[j+1],:,:,:] = data
        if verbose: print(j)
        j = j+1

    ##########################
    ########   end of reading
    ##########################
    if(verbose):
        print('hypercube', hypercube.shape, hypercube.min(), hypercube.max())
        ii = np.where( hypercube <= 0)
        print('number of negative pixels in hypercube', len(ii[0]))
        kk = np.where(np.isnan(hypercube))
        print('number of undefined pixels in hypercube', len(kk[0]))
              
    return hypercube
