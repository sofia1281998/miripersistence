# mirip_rebin

import numpy as np
import matplotlib.pyplot as plt
#######################
#print("rebin ")
def mirip_rebin1d(flux, bin_size,  verbose=False ):
    nt = len(flux)
    ni = nt//bin_size
    flux2=flux[0:ni*bin_size]
    flux3 = flux2.reshape([ni,bin_size])
    flux4 = np.nanmean(flux3, 1)
    return flux4
