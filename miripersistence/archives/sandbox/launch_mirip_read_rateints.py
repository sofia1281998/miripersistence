# IPython log file
from mirip_read_rateints import mirip_read_rateints

input_folder='/Users/gastaud/exoplanets/commissioning/pid1033_rateints/'

pattern='jw01033005001_04103_00001-seg00?_mirimage_rateints.*'

full, tt, target, pid = mirip_read_rateints(input_folder, pattern=pattern, verbose=True)

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib as mpl


mpl.rcParams['font.size'] = 4.0
mpl.rcParams['lines.linewidth'] = 0.5
mpl.rcParams['lines.markersize'] = 3


# Window size
x1= 11 #11
x2= 72 #72
y1= 0 #0
y2= 403

spectrum_zone = full[:, y1:y2,x1:x2]
x, y, z = spectrum_zone.shape
print(f"Chosen zone's shape is: {x}, {y} and {z}")

fig, ax = plt.subplots(figsize=(2, 3), dpi=300)

#im = ax.imshow(np.flip(full[0, y1:y2,x1:x2], axis=0), interpolation='nearest', aspect='auto', cmap=plt.cm.plasma, alpha=.9, norm=colors.LogNorm())

im = ax.imshow(spectrum_zone[-1, :,:], interpolation='nearest', aspect='auto', cmap=plt.cm.plasma, alpha=.9, norm=colors.LogNorm(), origin='lower')
plt.colorbar(im)
plt.title('Last Spectral Image for '+target+', pid:'+pid)
plt.savefig('my_subarray.png')


lightcurve = np.sum(full[:, 7:395, 36-5:36+6], (1,2))
plt.figure(figsize=(5, 2), dpi=300)
plt.scatter(time, lightcurve, s=10, alpha=0.5, color='blue')
plt.title('White light-curve for '+target+', pid:'+pid)
plt.xlabel('Time [s]')
plt.ylabel('Flux [DN/s]')
plt.tight_layout()
plt.savefig(os.path.join(output_folder, 'white.png'))


