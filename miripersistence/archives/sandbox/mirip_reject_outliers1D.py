#
#

import numpy as np
import matplotlib.pyplot as plt
import copy
from itertools import groupby
from operator import itemgetter
from matplotlib import colors
import matplotlib as mpl
import matplotlib.lines as lines


def mirip_reject_outliers1D(binned_lightcurve):
    l = len(binned_lightcurve)
    length = l - 2
    outliers = np.zeros(1)
    # Corrects the binned data, getting rid of the outlier values
    corrected_binned_lightcurve = copy.deepcopy(binned_lightcurve)

    while len(outliers) != 0:
        std_deviation = []
        outliers_ids = []
        outliers = []
        outlier_groups = []
        pre_ids = []
        post_ids = []

        print(f'Sigma clipping of 5 sigma with a moving mean of 5 values. ')

        for i in range(2, length):
            tmp_sd = np.std(binned_lightcurve[(i - 2):(i + 3)])
            std_deviation.append(tmp_sd)

        print(f'The moving std deviation vector length is: ',len(std_deviation))

        # Computes the running median value (5 per 5)
        running_neighbors = [binned_lightcurve[i - 2:i + 3] for i in range(2, length)]
        tmp_median = np.median(running_neighbors, axis=1)
        
        # The running medians array is reduced from 4 values

        # Adds zeros at the beginning and the end of the median values array
        # to go back to the lightcurve vector length
        median = np.zeros(l)
        median[2:length] = tmp_median

        print(f'The moving median vector length is: ',len(median))

        #  Gets the  median value  of the  moving standard deviation
        median_std = np.nanmedian(std_deviation)
        

        # Gets the outliers values and IDS
        for i in range(4, length):
            if np.abs(binned_lightcurve[i] - median[i]) > 5 * median_std:
                outliers.append(binned_lightcurve[i])
                outliers_ids.append(i)

        print(f'There are {len(outliers)} outliers in the binned lightcurve.')

        for k, g in groupby(enumerate(outliers_ids), lambda x: x[1] - x[0]):
            tmp_groups = np.asarray(list(map(itemgetter(1), g)))
            outlier_groups.append(tmp_groups)


        lower_limit = 0
        upper_limit = l - 1

        for group in outlier_groups:
            vector = group - len(group)
            vector[np.where(vector < lower_limit)[0]] = lower_limit
            pre_ids.append(vector)

        for group in outlier_groups:
            vector = group + len(group)
            vector[np.where(vector > upper_limit)[0]] = upper_limit
            post_ids.append(vector)


        #corrected_binned_lightcurve = np.zeros(len(binned_lightcurve))
        len_groups = len(outlier_groups)

        for i in range(len_groups):
            for j in range(len(outlier_groups[i])):
                corrected_binned_lightcurve[outlier_groups[i][j]] = 0.5 * (binned_lightcurve[pre_ids[i][j]] + binned_lightcurve[post_ids[i][j]])


        # Replaces the initial data by the corrected ones to search the remaining outliers
        binned_lightcurve = corrected_binned_lightcurve
        
    return corrected_binned_lightcurve

 
