# IPython log file
# Author : R Gastaud
# date :  20 April 2023
# From the cdp mask extract the subarray corresponding to MIRI LRS SLITLESS

from astropy.io import fits
import numpy as np
from matplotlib import pyplot as plt
import os
###############
input_dir = 'data/'
output_dir = 'data/'
mask_filename= 'jwst_miri_mask_0036.fits'
lrs_filename= 'MIRI_FM_MIRIMAGE_P750L_SLITLESSPRISM_DISTORTION_9B.00.08.fits'
###############
header = fits.getheader(os.path.join(input_dir, lrs_filename))
substr1 = header['SUBSTRT1']
substr2 = header['SUBSTRT2']
subsize1 = header['SUBSIZE1']
subsize2 = header['SUBSIZE2']
print(substr1, substr2, subsize1, subsize2)
mask = fits.getdata(os.path.join(input_dir,mask_filename))
print(mask.dtype, mask.min(), mask.max(), np.median(mask))

mask_header0 = fits.getheader(os.path.join(input_dir,mask_filename), 0)
print(repr(mask_header0))
mask_header1 = fits.getheader(os.path.join(input_dir,mask_filename), 1)
print(repr(mask_header1))

maskette = mask[substr2-1:substr2-1+subsize2, substr1-1:substr1-1+subsize1]

plt.imshow(maskette)
plt.show()

mask_header1['SUBSTRT1'] = substr1
mask_header1['SUBSTRT2'] = substr2
mask_header1['SUBSIZE1'] = subsize1
mask_header1['SUBSIZE2'] = subsize2

hdu0 = fits.PrimaryHDU(header=mask_header0)
hdu1 = fits.ImageHDU(maskette, header=mask_header1)
hdul = fits.HDUList([hdu0, hdu1])
hdul.writeto(os.path.join(output_dir,'maskette.fits'))
