# Laucnh mirip_rebin2D

import numpy as np
import matplotlib.pyplot as plt
from mirip_rebin2D import mirip_rebin2D
################
lightcurve = np.zeros([100,3])
lightcurve[:, 0] = np.sin(np.arange(100)*np.pi/50)
lightcurve[:, 1] = np.cos(np.arange(100)*np.pi/50)

binned_lightcurve = mirip_rebin2D(lightcurve, 10, plot=True, verbose=False )
plt.show()
