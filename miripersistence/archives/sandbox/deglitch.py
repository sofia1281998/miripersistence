# Deglitch: remove outliers


import numpy as np
from scipy.ndimage import median_filter
import matplotlib.pyplot as plt

def deglitch(raw_line, bin_size=10, k=3, verbose=False, plot=False):
    smoothed_line = median_filter(raw_line, bin_size)
    difference = smoothed_line - raw_line
    my_median = np.median(difference)
    my_deviation = np.median(np.abs(difference-my_median))
    thresold1 = my_median - k*1.4826*my_deviation
    thresold2 = my_median + k*1.4826*my_deviation
    ii = np.where(np.logical_or((difference > thresold2),(difference < thresold1)))
    ii = ii[0]
    if  verbose:print('number of glitches',len(ii))
    if plot:
        tt = np.arange(raw_line.size)
        plt.plot(raw_line)
        plt.plot(tt[ii], raw_line[ii], '.')
        plt.show()
    return ii
