# IPython log file

from fit_two_exponentials import fit_two_exponentials
import os
import numpy as np
from astropy.io import fits
##  initialisation
pid = '1033'
target ='L168-9'
input_folder = 'data/' # 'data/'
flux = np.loadtxt(os.path.join(input_folder, 'newY_data.txt'))
mask = np.loadtxt(os.path.join(input_folder, 'newY_mask.txt'))
mytime = np.loadtxt(os.path.join(input_folder, 'binned_time.txt'))
initial = np.array([ 4.334e+06,  1.000e+01,  1.000e+03, 33, 100])
# BEWARE DEPENDS UPON INITIAL CONDITION
# initial = np.array([ 4.334e+06,  1.000e+01,  1.000e+03, 10, 10])
fluxerr=0.001
###  end of initialisation
flux = np.loadtxt(os.path.join(input_folder, 'newY_data.txt'))
mask = np.loadtxt(os.path.join(input_folder, 'newY_mask.txt'))
mytime = np.loadtxt(os.path.join(input_folder, 'binned_time.txt'))
###################################
x = fit_two_exponentials(flux, mask, mytime, initial, fluxerr)
print(x)
###################################
x = fit_two_exponentials(flux, mask, mytime, initial, fluxerr, verbose=True)

######################  does not work (pid, target are missing)
# x = fit_two_exponentials(flux, mask, mytime, initial, fluxerr, verbose=True, plot=True)
