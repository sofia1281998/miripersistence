#!/bin/bash
#SBATCH -N 1 # Nbr of nodes
#SBATCH --time=12:00:00 # time (D-HH:MM)
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=achrene.dyrek@cea.fr # send-to address
#SBATCH --out=/feynman/home/dap/lde3/ah258874/work/data/WASP-43/persistence/%j.out

export PARENT=/feynman/home/dap/lde3/ah258874/work/data/WASP-43
export INPUT_DIR=$PARENT/pipeline_output
export CALCULATION_DIR=$PARENT/persistence

python show_lightcurve.py $INPUT_DIR $CALCULATION_DIR
