from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
import copy
import glob
from itertools import groupby
from operator import itemgetter
from matplotlib import colors
import matplotlib as mpl
import matplotlib.lines as lines
from scipy.optimize import minimize
from scipy.optimize import Bounds
import os
import sys

mpl.rcParams['font.size'] = 4.0
mpl.rcParams['lines.linewidth'] = 0.5
mpl.rcParams['lines.markersize'] = 3

####### TBD ########
input_folder = sys.argv[1] 
output_folder = sys.argv[2]

bin_size = 30
transit_start = 4800 #integration number
transit_stop = 7800 #integration_number
start = np.int32(np.rint(transit_start/bin_size))
stop = np.int32(np.rint(transit_stop/bin_size))

# Window size
x1= 11 #11 
x2= 72 #72 
y1= 0 #0
y2= 403

# Time array
time = np.arange(0, 9371*1.43136, 1.43136) #integration 0, last integration, integration time

# Fitting parameters: a + f * np.exp(-t/ tau + c) + d * t
# Order: a (DN/s), f (DN/s), tau (s), c (phase shift), d (1/s)

parameters = np.array((4.334e6, 10, 1000, 7, -0.1))
yerr = 1

###################

def persistence(parameters, t):
    a = parameters[0]
    f = parameters[1]
    tau = parameters[2]
    c = parameters[3]
    d = parameters[4]
    return a + f * np.exp(-t/ tau + c) + d * t



def log_likelihood(parameters, t, y, yerr):
    mod = persistence(parameters, t)
    
    sigma2 = yerr ** 2
    
    return -0.5 * np.sum((y - mod) ** 2 / sigma2)

##################

files = np.sort(glob.glob(os.path.join(input_folder, '*_rateints.fits')))

full = []

for file in files:  
    hdul = fits.open(file)
    data = hdul[1].data
    full.extend(data)
full = np.asarray(full, dtype=np.float32)


spectrum_zone = full[:, y1:y2,x1:x2]
x, y, z = spectrum_zone.shape
print(f"Chosen zone's shape is: {x}, {y} and {z}")

fig, ax = plt.subplots(figsize=(2, 3), dpi=300)

im = ax.imshow(np.flip(full[0, y1:y2,x1:x2], axis=0), interpolation='nearest', aspect='auto', cmap=plt.cm.plasma, alpha=.9, norm=colors.LogNorm())
plt.colorbar(im)
plt.savefig(os.path.join(output_folder, 'subarray.png'))

# White light-curve
lightcurve = np.sum(spectrum_zone, axis=(1, 2))


plt.figure(figsize=(5, 2), dpi=300)
plt.scatter(time, lightcurve, s=10, alpha=0.5, color='blue')
plt.xlabel('Time [s]')
plt.ylabel('Flux [DN/s]')
plt.title('White light-curve')
plt.savefig(os.path.join(output_folder, 'white.png'))

#Bin the data
print('Binning data.')
print(f'Bin size is {bin_size} values.')

# Gets the remainder of the floor division between lightcurve size and bin size
division_remainder = np.mod(len(lightcurve), bin_size)
#  9371 - (312*30+11)

# We  remove the points that could  not be  part of a full bin
# we remove the points at the begginning not at the end
tmp_data = lightcurve[division_remainder:]
# lightcurve shape 9360

binned_lightcurve = []
length = int(len(tmp_data) / bin_size)
#  length =  len(lightcurve)//bin_size
#  length 312

# We bin the data
for i in range(length):
    tmp_bin = np.mean(tmp_data[(i * bin_size):((i + 1) * bin_size)])
    binned_lightcurve.append(tmp_bin)

#  We bin the  time  vector
tmp_time = time[division_remainder:]
binned_time = []

for i in range(length):
    tmp_bin = np.mean(tmp_time[(i * bin_size):((i + 1) * bin_size)])
    binned_time.append(tmp_bin)


# Pixels time series
pixels_time_series = spectrum_zone
pixels_time_series = np.reshape(pixels_time_series, (x, y * z))

# Outlier detection

print("Sigma clipping")

l = len(binned_lightcurve)
length = l - 2
outliers = np.zeros(1)
# Corrects the binned data, getting rid of the outlier values
corrected_binned_lightcurve = copy.deepcopy(binned_lightcurve)

while len(outliers) != 0:
    std_deviation = []
    outliers_ids = []
    outliers = []
    outlier_groups = []
    pre_ids = []
    post_ids = []

    print(f'Sigma clipping of 5 sigma with a moving mean of 5 values. ')

    for i in range(2, length):
        tmp_sd = np.std(binned_lightcurve[(i - 2):(i + 3)])
        std_deviation.append(tmp_sd)

    print(f'The moving std deviation vector length is: ',len(std_deviation))

    # Computes the running median value (5 per 5)
    running_neighbors = [binned_lightcurve[i - 2:i + 3] for i in range(2, length)]
    tmp_median = np.median(running_neighbors, axis=1)  
    
    # The running medians array is reduced from 4 values

    # Adds zeros at the beginning and the end of the median values array 
    # to go back to the lightcurve vector length
    median = np.zeros(l)
    median[2:length] = tmp_median

    print(f'The moving median vector length is: ',len(median))

    #  Gets the  median value  of the  moving standard deviation
    median_std = np.nanmedian(std_deviation)
    

    # Gets the outliers values and IDS
    for i in range(4, length):
        if np.abs(binned_lightcurve[i] - median[i]) > 5 * median_std:
            outliers.append(binned_lightcurve[i])
            outliers_ids.append(i)

    print(f'There are {len(outliers)} outliers in the binned lightcurve.')

    for k, g in groupby(enumerate(outliers_ids), lambda x: x[1] - x[0]):
        tmp_groups = np.asarray(list(map(itemgetter(1), g)))
        outlier_groups.append(tmp_groups)


    lower_limit = 0
    upper_limit = l - 1

    for group in outlier_groups:
        vector = group - len(group)
        vector[np.where(vector < lower_limit)[0]] = lower_limit
        pre_ids.append(vector)

    for group in outlier_groups:
        vector = group + len(group)
        vector[np.where(vector > upper_limit)[0]] = upper_limit
        post_ids.append(vector)


    #corrected_binned_lightcurve = np.zeros(len(binned_lightcurve))
    len_groups = len(outlier_groups)

    for i in range(len_groups):
        for j in range(len(outlier_groups[i])):
            corrected_binned_lightcurve[outlier_groups[i][j]] = 0.5 * (binned_lightcurve[pre_ids[i][j]] + binned_lightcurve[post_ids[i][j]])


    # Replaces the initial data by the corrected ones to search the remaining outliers
    binned_lightcurve = corrected_binned_lightcurve
    
fig = plt.figure(figsize=(5, 2), dpi=300)
plt.scatter(binned_time, corrected_binned_lightcurve, marker='.', c='b')
plt.xlabel("BMJD")
plt.ylabel("Flux(DN/s)")
plt.title(f"{bin_size} values binned white-lightcurve sigma clipping (5 sigma)")
plt.savefig(os.path.join(output_folder, 'white_sigma_clipping.png'))

d, t = pixels_time_series.shape

reference = np.zeros(d)

corrected_binned_lightcurve = np.asarray(corrected_binned_lightcurve)

# Repeating the binned values to create a reference array

for i in range(bin_size):
    a = list(np.arange(l) * bin_size + i)
    for j in range(len(a)):
        b = a[j]
        reference[b] = corrected_binned_lightcurve[j]
    
# Including the last values that have been rejected to create the bins
division_remainder = np.mod(d, bin_size)
for i in range(division_remainder):
    reference[(l-1) * bin_size + bin_size + i] = corrected_binned_lightcurve[l - 1]


full_length = len(time) - 2
outliers = np.zeros(1)
lightcurve = np.sum(pixels_time_series, axis=1)

# Now Corrects the NON binned data, getting rid of the outlier values


corrected_lightcurve = copy.deepcopy(pixels_time_series)


while len(outliers) != 0:
    std_deviation = []
    outliers_ids = []
    outliers = []
    outlier_groups = []
    pre_ids = []
    post_ids = []
    
    for i in range(2, full_length):
        tmp_sd = np.std(lightcurve[(i - 2):(i + 3)])
        std_deviation.append(tmp_sd)

    print(f'The moving std deviation vector length is: ',len(std_deviation))

    #  Gets the  median value  of the  moving standard deviation
    median_std = np.nanmedian(std_deviation)
    # Gets the outliers values and IDS
    for i in range(2, full_length):
        if np.abs(lightcurve[i] - reference[i]) > 5.0 * median_std:
            outliers.append(lightcurve[i])
            outliers_ids.append(i)

    print(f'There are {len(outliers)} outliers in the full lightcurve.')


    for k, g in groupby(enumerate(outliers_ids), lambda x: x[1] - x[0]):
        tmp_groups = np.asarray(list(map(itemgetter(1), g)))
        outlier_groups.append(tmp_groups)
        
    lower_limit = 0
    upper_limit = len(time) - 1

    for group in outlier_groups:
        vector = group - len(group)
        vector[np.where(vector < lower_limit)[0]] = lower_limit
        pre_ids.append(vector)

    for group in outlier_groups:
        vector = group + len(group)
        vector[np.where(vector > upper_limit)[0]] = upper_limit
        post_ids.append(vector)


    #corrected_binned_lightcurve = np.zeros(len(binned_lightcurve))
    len_groups = len(outlier_groups)

    for i in range(len_groups):
        for j in range(len(outlier_groups[i])):
            corrected_lightcurve[outlier_groups[i][j], :] = 0.5 * (corrected_lightcurve[pre_ids[i][j], :] + corrected_lightcurve[post_ids[i][j], :])

    corrected_full_lightcurve = np.sum(corrected_lightcurve, axis=1)

    # Replaces the initial data by the corrected ones to search the remaining outliers
    lightcurve = corrected_full_lightcurve

# Gets the remainder of the floor division between lightcurve size and bin size
division_remainder = np.mod(len(time), bin_size)

# We  remove the points that could  not be  part of a full bin
tmp_data = corrected_lightcurve[division_remainder:, :]
a, b = tmp_data.shape

length = int(a / bin_size)
rebinned_pixels = np.zeros((length, b))
tmp_binned = np.zeros(b)

# We bin the data
for i in range(length):
    for j in range(b):
        tmp_bin = np.mean(tmp_data[(i * bin_size):((i + 1) * bin_size), j])
        tmp_binned[j] = tmp_bin
    rebinned_pixels[i, :] = tmp_binned
    
rebinned_lightcurve = np.mean(rebinned_pixels, axis=1)

e, f = rebinned_pixels.shape

# Removing data points during transit from the dataset
print('Removing data points during transit from the dataset.')

mask = np.full(e, False, dtype=bool)

mask[start:stop] = True 
mask_full = np.vstack([mask]*f)
newX = np.ma.masked_array(rebinned_pixels, mask_full.T)


twoDpixels_time_series = np.reshape(newX, (e, y, z))
#np.savetxt('masked_times_series.txt', (twoDpixels_time_series, binned_time[:, np.newaxis, np.newaxis]))
arr_reshaped = twoDpixels_time_series.reshape(twoDpixels_time_series.shape[0], -1)
np.savetxt('masked_times_series.txt', arr_reshaped)

#To retrieve the 3D array back while reading it:
#load_original_arr = loaded_arr.reshape(loaded_arr.shape[0], loaded_arr.shape[1] // arr.shape[2], arr.shape[2])

newY = np.ma.masked_array(rebinned_lightcurve, mask)


#fitting model
print("Fitting persistence model to data.")
print("Persistence model is: a + f * np.exp(-t/ tau + c) + d * t")

t = np.asarray(binned_time)

plt.figure(figsize=(5, 2), dpi=300)
plt.plot(t, persistence(parameters, t), 'b', label='initial model')
plt.xlabel('Time in days')
plt.ylabel('Flux in DN/s')
plt.title('Persistence model')
plt.legend()
plt.savefig(os.path.join(output_folder, 'persistence_model.png'))

# MLE on white-light curve

np.random.seed()

# Minimizing the negative likelihood function: same as maximizing it
nll = lambda *args: -log_likelihood(*args)

initial = parameters
                 
#bounds = Bounds(np.array(()))

print('White light-curve fit.')
print('Initial values:', initial)

soln = minimize(nll, initial, args=(t, newY, yerr), method='Nelder-mead', 
                options={'ftol' : 1e-8, 'maxiter' : 10000, 'disp' : True, 'adaptive' : True}) #bounds=bounds

params_mle = soln.x
conv = soln.success
print(f'Convergence: {conv}')
print("Maximum likelihood estimates:")
print(params_mle)

# MLE on all data
print('All pixels fitting.')

y_full = twoDpixels_time_series
convergence = []
a = []
f = []
tau = []
c = []
d = []
raw = []
column = []

for i in range(y):
    for j in range(z):
        soln = minimize(nll, initial, args=(t, y_full[:, i, j], yerr), method='Nelder-mead', 
                    options={'ftol' : 1e-8, 'maxiter' : 10000, 'disp' : False, 'adaptive' : True})
        params_mle = soln.x
        conv = soln.success
        convergence.append(conv)
        a.append(params_mle[0])
        f.append(params_mle[1])
        tau.append(params_mle[2])
        c.append(params_mle[3])
        d.append(params_mle[4])
        raw.append(i)
        column.append(j)

#return a + f * np.exp(-t/ tau + c) + d * t
#bins Elsa: wave_low [5.168, 5.327, 5.645, 5.805, 5.964, 6.123, 6.282, 6.441, 6.6, 6.759, 6.918, 7.077, 7.236, 7.395
#7.555, 7.714, 7.873, 8.032, 8.191, 8.35, 8.509, 8.668, 8.827, 8.986, 9.145, 9.305, 9.464, 9.623, 9.782, 9.941, 10.1, 
#10.259, 10.418, 10.577, 10.736, 10.895, 11.055, 11.214, 11.373, 11.532]

#wave_high [5.327, 5.645, 5.805, 5.964, 6.123, 6.282, 6.441, 6.6, 6.759, 6.918, 7.077, 7.236, 7.395
#7.555, 7.714, 7.873, 8.032, 8.191, 8.35, 8.509, 8.668, 8.827, 8.986, 9.145, 9.305, 9.464, 9.623, 9.782, 9.941, 10.1, 
#10.259, 10.418, 10.577, 10.736, 10.895, 11.055, 11.214, 11.373, 11.532, 11.691]

convergence = np.asarray(convergence)
ind = np.where(convergence == True)

a = np.asarray(a, dtype=np.int32)
f = np.asarray(f, dtype=np.int32)
tau = np.asarray(tau, dtype=np.int32)
c = np.asarray(c, dtype=np.int32)
d = np.asarray(d, dtype=np.int32)
raw = np.asarray(raw, dtype=np.int32)
column = np.asarray(column, dtype=np.int32)

good_a = a[ind]
good_f = f[ind]
good_tau = tau[ind]
good_c = c[ind]
good_d = d[ind]
good_raw = raw[ind]
good_column = column[ind]

print('Parameters saved: a, f, tau, c, d, raw number, column number.')

np.savetxt('parameters.txt', (good_a, good_f, good_tau, good_c, good_d, good_raw, good_column))

fig, axs = plt.subplots(2, 3, figsize=(5.5, 4), dpi=300)
axs[0, 0].scatter(good_a, good_f, marker='.', c='b')
axs[0, 0].set_title("f vs a")
axs[1, 0].scatter(good_a, good_tau, marker='.', c='b')
axs[1, 0].set_title("tau vs a")
axs[0, 1].scatter(good_raw, good_f, marker='.', c='b')
axs[0, 1].set_title("f vs raw")
axs[1, 1].scatter(good_raw, good_tau, marker='.', c='b')
axs[1, 1].set_title("tau vs raw")
axs[0, 2].scatter(good_column, good_f, marker='.', c='b')
axs[0, 2].set_title("f vs column")
axs[1, 2].scatter(good_column, good_tau, marker='.', c='b')
axs[1, 2].set_title("tau vs column")
plt.savefig(os.path.join(output_folder, 'parameters.png'))

