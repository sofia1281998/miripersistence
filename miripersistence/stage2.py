# stage2: remove transit/eclipse



# imports
import logging


LOG = logging.getLogger('stage2')


#def get_transit_eclipse():
#
#    return
#def mask_transit_eclipse():
#
#    return masked_data

def stage2():

    # data is a 3D array of flux (raw, column, time)
    # call get_transit_eclipse() on white light-curve
    # call mask_transit_eclipse() on data array
    # write results into result_folder
    # write figs into result_folder/figs
    # masked_data is a 3D array of flux (raw, column, time)
    LOG.info('You are in stage 2.')

    return
