import logging
logging.getLogger('main').addHandler(logging.NullHandler())

__version__ = '0.1.0'
__author__ = 'Achrène Dyrek'
__description__ = 'Module for MIRI LRS persistence effects investigation.'
__email__ = 'achrene.dyrek@cea.fr'

from . import utils
from . import config
from . import stage1
from . import stage2
from . import stage3
from . import stage4
from . import stage5
from . import main
from . import compute_t2_t3
