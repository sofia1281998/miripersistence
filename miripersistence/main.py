"""
Parses the config.ini arguments

@author: A Dyrek

:History:
    March 05 2023: Created file

================================================================
"""
import sys
from configobj import ConfigObj
import os
import logging
import numpy as np

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

from miripersistence import config
from miripersistence.stage1 import stage1
from miripersistence.stage2 import stage2
from miripersistence.stage3 import stage3
from miripersistence.stage4 import stage4
from miripersistence.stage5 import stage5
from miripersistence import utils

LOG = logging.getLogger('main')

def run(config_file):

    # parser
    if isinstance(config_file, str):
        configuration = config.get_config(config_file)
    else:
        configuration = config_file

    # check sections
    if not ('general' in configuration.keys()):
        LOG.error("'general' section is missing in configuration file")
        sys.exit()

    if not ('stage1' in configuration.keys()):
        LOG.error("'stage1' section is missing in configuration file")
        sys.exit()

    if not ('stage2' in configuration.keys()):
        LOG.error("'stage2' section is missing in configuration file")
        sys.exit()

    if not ('stage3' in configuration.keys()):
        LOG.error("'stage3' section is missing in configuration file")
        sys.exit()

    if not ('stage4' in configuration.keys()):
        LOG.error("'stage4' section is missing in configuration file")
        sys.exit()

    if not ('stage5' in configuration.keys()):
        LOG.error("'stage5' section is missing in configuration file")
        sys.exit()

    # general
    target_name = configuration['general']['target_name']
    input_folder = configuration['general']['input_folder']
    output_folder = configuration['general']['output_folder']
    x_window = np.array(configuration['general']['x_window']) # we could add extra verif: >0, array[0] < array[1], array[1]<lim
    y_window = np.array(configuration['general']['y_window']) # we could add extra verif: >0, array[0] < array[1], array[1]<lim
    LOG.info(f'General params are {target_name}, {input_folder}, {output_folder}, {x_window}, {y_window}')

    # stages
    run_stage_1 = configuration['stage1']['run_stage_1']
    run_stage_2 = configuration['stage2']['run_stage_2']
    run_stage_3 = configuration['stage3']['run_stage_3']
    run_stage_4 = configuration['stage4']['run_stage_4']
    run_stage_5 = configuration['stage5']['run_stage_5']
    LOG.info(f'Stages are: {run_stage_1}, {run_stage_2}, {run_stage_3}, {run_stage_4}, {run_stage_5}')

    # initialisation of output folder
    #result_folder = create_folder(target_name, output_folder) # we have to define create_folder in utils.py + result folder must contain target name + date + result folder must contain a figs folder
    LOG.info('Here is the initialisation step')
    # get files and subbaray only if stages 1, 2 and 3 are active
    test = 0

    for stage in [run_stage_1, run_stage_2, run_stage_3]:
        if stage is True:
            test += 1 # we could check if all stages are false and print an error
    LOG.info(f'Test value is {test}')
    if test == 0:
        LOG.info('This a telemetry retreival only.')
    else:
        LOG.info('Getting the data')
        #data = get_data(input_folder) # we have to define get_data in utils.py
        #data = get_subarray(data, x_window, y_window, result_folder) # we have to define get_subarray in utils.py + write data in result_folder

    # stage1
    if run_stage_1 is True:
        LOG.info('Starting stage1')
        bin_size = configuration['stage1']['bin_size'] # we could add extra verif: <integration_number
        LOG.info(f'Bin size is {bin_size}')
        stage1()
        LOG.info('stage1 executed')

    # stage2    
    if run_stage_2 is True:
        LOG.info('Starting stage2')
        stage2()
        LOG.info('stage2 executed')

    # stage3 
    if run_stage_3 is True:
        LOG.info('Starting stage3')
        model = configuration['stage3']['model']
        first_guess = np.array(configuration['stage3']['first_guess'])
        LOG.info(f'Model and first guess are {model}, {first_guess}')
        stage3()
        LOG.info('stage3 executed')

    # stage4
    if run_stage_4 is True:
        if run_stage_3 is False:
            LOG.error('Cannot run stage 4 without runnning stage 3.')
        else:
            LOG.info('Starting stage4')
            value_to_remove = configuration['stage4']['value_to_remove'] # we could add extra verif <signal_max
            LOG.info(f'Value to remove is {value_to_remove}')
            stage4()
            LOG.info('stage4 executed')

    # stage5
    if run_stage_5 is True:
        LOG.info('Starting stage5')
        telemetry_start_date = configuration['stage5']['telemetry_start_date']
        telemetry_end_date = configuration['stage5']['telemetry_end_date']
        mnemonics =  configuration['stage5']['mnemonics']
        nframes = configuration['stage5']['nframes']
        LOG.info(f'Stage 5 params are {telemetry_start_date}, {telemetry_end_date}, {mnemonics}, {nframes}')
        stage5()
        LOG.info('stage5 executed')

    LOG.info('Process finished')
if __name__ == '__main__':
    utils.init_log(log="miripersistence.log", stdout_loglevel="INFO", file_loglevel="DEBUG")
    run('/feynman/work/dap/lde3/ah258874/miripersistence/miripersistence/config.ini')
