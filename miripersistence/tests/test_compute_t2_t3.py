import numpy as np
from miripersistence.compute_t2_t3 import compute_t2_t3

def test_compute_t2_t3():
    P = 1.40150
    rp_div_rs = 0.0212
    a_div_rs = 7.61
    i = 85.5
    result = 3947.38
    t2_t3 = compute_t2_t3(P, rp_div_rs, a_div_rs, i, verbose=True)
    np.testing.assert_allclose(t2_t3, result, rtol=1e-4, atol=0.1)
    return
