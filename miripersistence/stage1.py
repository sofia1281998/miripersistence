# stage1: sigma-clipping


# imports
import logging


LOG = logging.getLogger('stage1')

#def binning():
#
#    return

#def sigma_clipping(binned_data, data):
#
#    return data

def stage1():
   
    # data is a 3D array of flux (raw, column, time)
    # call binning
    # call sigma_clipping
    # write results into result_folder
    # write figs into result_folder/figs
    # data is a 3D array of flux (raw, column, time)
    LOG.info('You are in stage 1.')

    return
