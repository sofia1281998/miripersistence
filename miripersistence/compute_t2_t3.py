# IPython log file

import numpy as np

def compute_t2_t3(P, rp_div_rs, a_div_rs, i, verbose=False ):
    """Transit duration  T23 from P, rp_div_rs, a_div_rs, i.

    Calculates the transit duration (T23) from the orbital period, planet-star radius ratio, scaled semi-major axis, orbital inclination.
    From final_thesis_Giuseppe_Morello, formula 3.5
     "A machine-learning approach to exoplanet spectroscopy"
     
     Parameters
     ----------

       P  : orbital period         [day]
       rp_div_rs  : planet radius divided by star radius  [dimensionless]
       a_div_rs  : semi-major axis divided by star radius  [dimensionless]
       i  : orbital inclination    [degree]
       verbose : flag, if true print info
     Returns
     -------

        t_2_3  : transit duration Tii to Tiiii [second]
     """
    ir = np.radians(i)
    if(verbose):print(P, rp_div_rs, a_div_rs, i, ir)
    impact_parameter = a_div_rs*np.cos(ir)
    A = (1-rp_div_rs)**2 - impact_parameter**2
    B = a_div_rs*np.sin(ir)
    t_2_3 = P/np.pi*np.arcsin(np.sqrt(A)/B)
    t_2_3 =  t_2_3*24*3600
    if(verbose):print('t_2_3 ', t_2_3 , 'second')
    return t_2_3
